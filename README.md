# share-my-todos

This project contains scripts used for Share My Todos issues used when team member is out of office and we ask each other to take care of our todos while we are out.

![Share My Todo Example](images/example.png)

## Instructions

1. [Create new private project](https://gitlab.com/projects/new#blank_project)
1. [Create new personal access token](https://gitlab.com/-/user_settings/personal_access_tokens) with the `api` scope. You will use the value of the token in the following steps.
1. [Add these CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project) to your project:
  * `GITLAB_TOKEN`: The token value you created in the previous step. Mark the variable as masked and protected.
  * `TODO_VIEWER_IDS`: The ids of users (comma-separated) you'd like to share your todos with. This can be found using API with https://gitlab.com/api/v4/users?username=alan or in the user profile by clicking on `⋮` on the right side of the page. This is used to hide todos that cannot be resolved by selected team members.
  * `TODO_USERNAME`: Your GitLab username.
1. Create a `.gitlab-ci.yml` file with the following contents:
   ```
   include:
   - https://gitlab.com/alan/share-my-todos/-/raw/main/templates/.gitlab-ci.yml
   ```
1. Make sure your project has an issue matching the `TODO_ISSUE_IID` from `share_my_todos.rb` (i.e., create an empty issue with ID 1)
1. [Create a new pipeline schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule) (hourly) and add the variable `UPDATE_SHARE_MY_TODOS` set to `true`.
1. Test your `share my todos` project by [running the schedule pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#run-manually). You should see data in the issue matching the `TODO_ISSUE_IID` from `share_my_todos.rb`.
1. Add members to your project to share your todos.
