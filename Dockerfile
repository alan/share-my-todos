FROM ruby:3.2-alpine

RUN apk add --no-cache glab

RUN mkdir /gitlab
WORKDIR /gitlab

COPY share_my_todos.rb ./

SHELL ["/bin/bash", "-c"]
CMD ["ruby", "share_my_todos.rb"]

